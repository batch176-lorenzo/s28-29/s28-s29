// [OBJECTIVE] Create a server - side app using Express Web Framework.

// append the entire app to our node package manager.
	// package.json -> the "heart" of every node project. this also contains different metadata that describes the structure of the project.

	// scripts -> is used to declare and descrivbe custom commands and keyword that can be used to execute this project with the correct runtime environment

	// NOTE : "start" is globally recognize amongst node projects and frameworks as the 'default' commant script to execute a task/project
	// however for *unconventional* keywords or command you have to append the comman "run"
	// SYNTAX: npm run <custom command>

// Identify and prepare
const express = require("express");
// express => will be used as the main component to create the server.
//we need to be able to gather/acquire the utilities and components needed that the express library will provide us.
	// => require() -> directive used to get the library/component needed inside the module.

	// prepare the environment in which the project will be served

//Preparing a Remote repository for our Node project.
	//NOTE: always DISABLE the node_modules folder
	//WHY? -> it will take up to much space in out repository making a lot more difficult to stage upon commiting the changes in our remote repository
		// -> if ever that you will deploy you node project on deployment platforms (heroku, netlify, vercel) ther project will automatically be rejected, because node_modules is not recognized on various deployment platforms.
	//HOW? using a .gitignore module	
// 
console.log(`

	Welcome to our Express API Server
	______¶¶
	______¶¶______________¶¶¶¶¶
	______¶¶¶____________¶¶¶¶¶¶¶
	______¶¶¶____________¶¶¶¶¶¶¶¶
	_____¶¶¶¶___________¶¶¶¶¶¶¶¶¶
	___¶¶¶¶¶¶¶__________¶¶¶¶¶¶¶¶¶¶
	___¶¶¶¶_____________¶¶¶¶¶¶¶¶¶¶
	____¶¶_______________¶¶¶¶¶¶¶¶¶¶
	____¶¶_____________¶¶¶¶¶¶¶¶¶¶¶
	_____¶¶_______________¶¶¶¶¶¶¶
	_____¶¶__________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶
	_____¶¶¶_______¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶
	_____¶¶¶___¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶
	______¶¶¶¶¶¶¶¶¶¶¶__¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶
	______¶¶¶¶¶¶¶¶____¶¶¶¶¶¶¶¶¶¶¶¶¶___¶¶¶¶
	__________________¶¶¶¶¶¶¶¶¶¶¶¶¶____¶¶¶¶
	___________________¶¶¶¶¶¶¶¶¶¶¶______¶¶¶¶
	____________________¶¶¶¶¶¶¶¶¶¶_______¶¶¶
	___________________¶¶¶¶¶¶¶¶¶¶_________¶¶¶
	__________________¶¶¶¶¶¶¶¶¶¶¶_________¶¶¶
	_________________¶¶¶¶¶¶¶¶¶¶¶¶__________¶¶
	________________¶¶¶¶¶¶¶¶¶¶¶¶¶__________¶¶
	_______________¶¶¶¶¶¶¶¶¶¶¶¶¶¶___________¶¶
	______________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶___________¶¶
	_____________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶___________¶¶¶
	____________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶___________¶_¶¶
	____________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶_____________¶¶
	___________¶¶¶¶¶¶¶¶____¶¶¶¶¶¶¶¶
	___________¶¶¶¶¶¶¶______¶¶¶¶¶¶¶¶
	__________¶¶¶¶¶¶¶________¶¶¶¶¶¶¶
	__________¶¶¶¶¶¶__________¶¶¶¶¶¶¶
	_________¶¶¶¶¶¶____________¶¶¶¶¶¶
	_________¶¶¶¶¶______________¶¶¶¶¶¶
	________¶¶¶¶¶________________¶¶¶¶¶¶
	_______¶¶¶¶¶__________________¶¶¶¶¶¶
	______¶¶¶¶¶_____________________¶¶¶¶¶¶
	_____¶¶¶¶¶_______________________¶¶¶¶¶¶
	_____¶¶¶¶¶________________________¶¶¶¶¶¶
	____¶¶¶¶¶__________________________¶¶¶¶¶¶
	____¶¶¶¶____________________________¶¶¶¶¶¶
	___¶¶¶¶_______________________________¶¶¶¶
	___¶¶¶_________________________________¶¶¶¶
	___¶¶¶__________________________________¶¶¶
	__¶¶¶____________________________________¶¶¶
	_¶¶¶_____________________________________¶¶¶
	¶¶¶¶______________________________________¶¶¶
	__________________________________________¶¶¶

	`)
